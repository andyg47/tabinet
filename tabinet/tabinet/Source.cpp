﻿#include <iostream>
#include <string>
#include "Tabinet.h"

int main()
{
	int nr;
	auto ok = false;
	std::cout << "PROIECT TABINET" << std::endl;
    std::cout << std::endl;
	std::cout << "Introduceti numarul de jucatori: ";
	std::cin >> nr;
	if (nr == 2 || nr == 3 || nr == 4)
	{
		ok = true;
	}
	while (!ok)
	{
		std::cout << "Input gresit, introduceti numarul de jucatori (2-4)" << std::endl;
		std::cin >> nr;
		if (nr == 2 || nr == 3 || nr == 4)
		{
			ok = true;
		}
	}

	Tabinet T(nr);
	T.run();

	
	
}